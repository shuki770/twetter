package com.webapps770.tweet_flow.db;

import android.util.Log;

import com.webapps770.tweet_flow.models.Tweet;
import com.webapps770.tweet_flow.models.UserTag;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

/**
 * Created by shuki on 13/03/2019.
 */

public  class UserTagDbProvider {
    public static AppDatabase db = AppDatabase.getInstance();


    public static Single<List<String>> getAll() {
        return Single.create(emitter -> {
            List<String> userTags = new ArrayList<>();

            for (UserTag tag: db.userTagsDao().getAll())  {
                userTags.add(tag.getTagName());
            }

            emitter.onSuccess(userTags);
        });
    }

    public static Single<List<String>> upsertAll(List<String> tags) {
        return Single.create(emitter -> {
            List<UserTag> userTags = new ArrayList<>();

            for (String tag: tags)  {
                userTags.add(new UserTag(tag));
            }

            db.userTagsDao().removeAll();
            db.userTagsDao().insertAll(userTags);

            emitter.onSuccess(tags);
        });
    }

    public static Single removeAll() {
        return Single.create(emitter -> {
            db.userTagsDao().removeAll();
            emitter.onSuccess(new Object());
        });
    }
}