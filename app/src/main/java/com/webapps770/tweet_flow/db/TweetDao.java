package com.webapps770.tweet_flow.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.webapps770.tweet_flow.models.Tweet;

import java.util.List;

/**
 * Created by shuki on 05/03/2019.
 */

@Dao
public interface TweetDao {
    @Query("SELECT * FROM tweet")
    List<Tweet> getAll();

    @Query("SELECT * FROM tweet WHERE tag IN (:tags)")
    List<Tweet> getByTags(List<String> tags);

    @Query("SELECT * FROM tweet WHERE created_by LIKE :username")
    List<Tweet> findByUser(String username);

    @Query("SELECT * FROM tweet WHERE id LIKE :id")
    LiveData<List<Tweet>> findById(String id);

    @Query("SELECT * FROM tweet WHERE id LIKE :tag")
    List<Tweet> findByTag(String tag);

    @Delete
    void delete(Tweet tweet);

    @Query("DELETE FROM tweet")
    void removeAll();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<Tweet> tweets);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateAll(List<Tweet> tweets);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(Tweet tweet);
}