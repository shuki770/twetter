package com.webapps770.tweet_flow.db;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Created by shuki on 05/03/2019.
 */

public class Converters {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}