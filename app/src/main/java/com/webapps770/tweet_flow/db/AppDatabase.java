package com.webapps770.tweet_flow.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.webapps770.tweet_flow.models.Tweet;
import com.webapps770.tweet_flow.models.UserTag;

/**
 * Created by shuki on 05/03/2019.
 */

@Database(entities = {Tweet.class, UserTag.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    private static volatile AppDatabase INSTANCE;

    public abstract TweetDao tweetDao();
    public abstract UserTagsDao userTagsDao();

    public static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "tweeter.db")
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public static AppDatabase getInstance() {
        return INSTANCE;
    }
}
