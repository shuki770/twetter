package com.webapps770.tweet_flow.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.webapps770.tweet_flow.models.Tweet;
import com.webapps770.tweet_flow.models.UserTag;

import java.util.List;

/**
 * Created by shuki on 13/03/2019.
 */
@Dao
public interface UserTagsDao {
    @Query("SELECT * FROM user_tags")
    List<UserTag> getAll();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<UserTag> userTags);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateAll(List<UserTag> userTags);

    @Query("DELETE FROM user_tags")
    void removeAll();
}
