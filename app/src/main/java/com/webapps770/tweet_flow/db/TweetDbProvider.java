package com.webapps770.tweet_flow.db;

import android.util.Log;

import com.webapps770.tweet_flow.models.Tweet;


import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by shuki on 10/03/2019.
 */

public  class TweetDbProvider {
    public static AppDatabase db = AppDatabase.getInstance();


    public static Single<List<Tweet>> getAll() {
        return Single.create(emitter -> {
            List<Tweet> tweets = db.tweetDao().getAll();
            emitter.onSuccess(tweets);
        });
    }

    public static Single<List<Tweet>> getByTags(List<String> tags) {
        return Single.create(emitter -> {
            List<Tweet> tweets;
            if (tags.size() > 0) {
                tweets = db.tweetDao().getByTags(tags);
            } else {
                tweets = db.tweetDao().getAll();
            }

            emitter.onSuccess(tweets);

        });
    }



    public static Single<List<Tweet>> getByTag(String tag) {
        return Single.create(emitter -> {
            List<Tweet> tweets = db.tweetDao().findByTag(tag);
            emitter.onSuccess(tweets);
        });
    }


    public static Single<List<Tweet>> upsertAll(List<Tweet> newTweets) {
        return Single.create(emitter -> {
            db.tweetDao().removeAll();
            db.tweetDao().insertAll(newTweets);

            emitter.onSuccess(newTweets);
        });
    }
}
