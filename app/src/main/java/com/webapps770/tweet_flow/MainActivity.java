package com.webapps770.tweet_flow;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.webapps770.tweet_flow.activities.TagListActivity;
import com.webapps770.tweet_flow.db.AppDatabase;
import com.webapps770.tweet_flow.db.UserTagDbProvider;
import com.webapps770.tweet_flow.fragments.AddTweetFragment;
import com.webapps770.tweet_flow.fragments.LoginFragment;
import com.webapps770.tweet_flow.fragments.TweetsFragment;
import com.webapps770.tweet_flow.services.TagsService;
import com.webapps770.tweet_flow.services.TweetService;

import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AddTweetFragment.OnFragmentInteractionListener, TweetsFragment.OnFragmentInteractionListener, LoginFragment.LoginFragmentListener {

    private Menu navMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppDatabase db = AppDatabase.getInstance(this);
        TagsService.getTags().take(1).subscribe();
        TweetService.init();
        UserTagDbProvider.getAll()
                .subscribeOn(Schedulers.io())
                .subscribe(TagsService.userTags$::onNext);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navMenu = navigationView.getMenu();


        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, LoginFragment.newInstance())
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, TweetsFragment.newInstance())
                    .commit();
            showUser(FirebaseAuth.getInstance().getCurrentUser());
                navMenu
                    .findItem(R.id.nav_log_out)
                    .setVisible(true);
        }

//        EditText editText = (EditText) findViewById(R.id.editText);
//        String message = editText.getText().toString();
//        intent.putExtra(EXTRA_MESSAGE, message);

    }

    private void showUser(FirebaseUser currentUser) {

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View v = navigationView.getHeaderView(0);
        TextView userName = v.findViewById(R.id.user_name);
        userName.setText(currentUser.getDisplayName());
        TextView userEmail = v.findViewById(R.id.user_email);
        userEmail.setText(currentUser.getEmail());

        ImageView userImage = v.findViewById(R.id.user_image);
        Glide.with(this)
                .load(currentUser.getPhotoUrl())
                .apply(RequestOptions.circleCropTransform())
                .into(userImage);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            // findViewById(R.id.fab).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menu_tag_list) {
            Intent intent = new Intent(this, TagListActivity.class);
            startActivityForResult(intent, 0);
        } else if (id == R.id.nav_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "This message send from TweetFlow APP!");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_log_out) {
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                FirebaseAuth.getInstance().signOut();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content, LoginFragment.newInstance())
                        .commit();

                navMenu
                        .findItem(R.id.nav_log_out)
                        .setVisible(false);
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onLoginSuccess() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, TweetsFragment.newInstance())
                .commit();
        showUser(FirebaseAuth.getInstance().getCurrentUser());
        navMenu
                .findItem(R.id.nav_log_out)
                .setVisible(true);
    }
}
