package com.webapps770.tweet_flow.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.webapps770.tweet_flow.R;
import com.webapps770.tweet_flow.adapters.TweetsAdapter;
import com.webapps770.tweet_flow.services.TweetService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TweetsFragment extends Fragment {
    private OnFragmentInteractionListener mListener;

    public TweetsFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static TweetsFragment newInstance() {
        TweetsFragment fragment = new TweetsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tweets, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton fab = view.findViewById(R.id.fab_add_tweet);
        fab.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, AddTweetFragment.newInstance())
                    .addToBackStack(AddTweetFragment.class.getSimpleName())
                    .commit();
        });

        RecyclerView rcTweets = view.findViewById(R.id.rv_tweets);
        rcTweets.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        TweetService.getTweets()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tweets -> {
                    TweetsAdapter adapter = new TweetsAdapter(tweets);
                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    rcTweets.setLayoutManager(llm);
                    rcTweets.setAdapter(adapter);
                }, err -> {
                    Toast.makeText(getContext(), err.getMessage(), Toast.LENGTH_LONG).show();
                    System.out.print(err.getMessage());
                });

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {

    }
}
