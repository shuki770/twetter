package com.webapps770.tweet_flow.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.webapps770.tweet_flow.MainActivity;
import com.webapps770.tweet_flow.R;
import com.webapps770.tweet_flow.models.Tag;
import com.webapps770.tweet_flow.models.Tweet;
import com.webapps770.tweet_flow.services.FirebaseService;
import com.webapps770.tweet_flow.services.TagsService;
import com.webapps770.tweet_flow.services.TweetService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AddTweetFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private static final int PICK_IMAGE = 345;
    private static final int REQUEST_IMAGE_CAPTURE = 346;

    private String tweetTag;

    private OnFragmentInteractionListener mListener;
    private Uri imageUri;
    private ProgressBar loader;
    private ImageView new_tweet_img_preview;

    private String currentPhotoPath;

    public AddTweetFragment() {
        // Required empty public constructor
    }

    public static AddTweetFragment newInstance() {
        AddTweetFragment fragment = new AddTweetFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_tweet, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loader = view.findViewById(R.id.send_loading);
        new_tweet_img_preview = view.findViewById(R.id.new_tweet_img_preview);
        Spinner spinner = view.findViewById(R.id.tweet_tag_spinner);
        List<String> tags = new ArrayList<>();
        tags.add(getString(R.string.choose_tag));
        for (Tag tag : TagsService.tags) {
            tags.add(tag.getName());
        }

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, tags);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);


        view.findViewById(R.id.btn_choose_img).setOnClickListener(v -> {

            AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                    .setTitle(getString(R.string.choose_image))
                    .setMessage(R.string.choose_image_message)
                    .setPositiveButton(getString(R.string.camera), (dialog, which) -> {

                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                            // Create the File where the photo should go
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException ex) {
                                // Error occurred while creating the File
                            }
                            // Continue only if the File was successfully created
                            if (photoFile != null) {
                                imageUri = FileProvider.getUriForFile(getActivity(),
                                        "com.example.android.fileprovider",
                                        photoFile);
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                            }
                        }

                    }).setNegativeButton(getString(R.string.gallery), (dialog, which) -> {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
                    }).create();
            alertDialog.show();
        });

        Button submit = view.findViewById(R.id.btn_send);
        submit.setOnClickListener(v -> {
            submit.setEnabled(false);
            loader.setVisibility(View.VISIBLE);
            EditText input = view.findViewById(R.id.input_tweet_text);
            String message = input.getText().toString();

            if (TextUtils.isEmpty(message)) {
                Toast.makeText(getContext(), getString(R.string.tweet_message_empty), Toast.LENGTH_LONG).show();
                loader.setVisibility(View.GONE);
                submit.setEnabled(true);

                return;
            }

            if (imageUri != null) {
                TweetService.uploadImage(imageUri)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(pathUri -> {
                            addTweet(message, pathUri);
                        });
            } else {
                addTweet(message, null);
            }
        });
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
        tweetTag =  (String) parent.getItemAtPosition(pos);
    }

    public void onNothingSelected(AdapterView<?> parent) {
        tweetTag = null;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void addTweet(String message, Uri imagePath) {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Tweet tweet = new Tweet(message, null);
        tweet.setCreatedDate(Calendar.getInstance().getTime());
        tweet.setCreatedBy(user.getDisplayName());
        tweet.setUserId(user.getUid());
        tweet.setUserPhoto(user.getPhotoUrl().toString());

        if (imagePath != null) {
            tweet.setImageUrl(imagePath.toString());
        }

        if (!TextUtils.isEmpty(tweetTag) && tweetTag != getString(R.string.choose_tag)) {
            tweet.setTag(tweetTag);
        }

        TweetService.addTweet(tweet)
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Toast.makeText(getActivity(), getString(R.string.error_message), Toast.LENGTH_LONG).show();
                    }
                    
                    loader.setVisibility(View.GONE);
                    getActivity().onBackPressed();
                });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            imageUri = data.getData();
            Glide.with(this).load(imageUri).into(new_tweet_img_preview);
        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
                if (imageUri != null) {
                    Glide.with(this).load(imageUri).into(new_tweet_img_preview);
                }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

    }
}
