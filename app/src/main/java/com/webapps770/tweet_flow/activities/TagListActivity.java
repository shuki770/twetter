package com.webapps770.tweet_flow.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import 	android.widget.CheckBox;
import 	android.widget.CompoundButton.OnCheckedChangeListener;
import com.webapps770.tweet_flow.R;
import com.webapps770.tweet_flow.adapters.TagAdapter;
import com.webapps770.tweet_flow.services.TagsService;

public class TagListActivity extends AppCompatActivity {
    private final int TAG_FORM = 1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == TAG_FORM && resultCode == RESULT_OK) {
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(v -> {
                    Intent intent = new Intent(this, TagFormActivity.class);
                    // startActivity(intent);
                    startActivityForResult(intent, TAG_FORM);

//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show();
                });


        RecyclerView rcTags = findViewById(R.id.rv_tags);
        rcTags.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));


        TagsService.getTags()
                .subscribe(tag -> {
                    TagAdapter adapter = new TagAdapter(tag);
                    LinearLayoutManager llm = new LinearLayoutManager(this);
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    rcTags.setLayoutManager(llm);
                    rcTags.setAdapter(adapter);
                }, err -> {
                    System.out.print(err.getMessage());
                });



    }






    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }




}
