package com.webapps770.tweet_flow.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.webapps770.tweet_flow.R;
import com.webapps770.tweet_flow.models.Tag;
import com.webapps770.tweet_flow.services.TagsService;


public class TagFormActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_form);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ProgressBar loader = findViewById(R.id.add_tag_loading);
        loader.setVisibility(View.GONE);

        Button button = (Button) findViewById(R.id.btn_tag_submit);
        button.setOnClickListener(view -> {

            String name = ((EditText)findViewById(R.id.input_tag_name)).getText().toString();

            if (TextUtils.isEmpty(name)) {
                Toast.makeText(this, getString(R.string.name_empty), Toast.LENGTH_LONG).show();

                return;
            }

            for (Tag tag: TagsService.tags) {
                if (tag.getName().equals(name)) {
                    Toast.makeText(this, getString(R.string.tag_exist), Toast.LENGTH_LONG).show();
                    return;
                }
            }

            Tag newTag = new Tag(name);
            button.setEnabled(false);

            loader.setVisibility(View.VISIBLE);
            TagsService.addTag(newTag)
            .addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    finish();
                } else {
                    Toast.makeText(this, getString(R.string.error_message), Toast.LENGTH_LONG).show();
                }

                loader.setVisibility(View.GONE);
            });
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
