package com.webapps770.tweet_flow.models;

enum UserProvider {
    google,
    local
}

public class User {
    public String id;
    public String email;
    public UserProvider provider;
    public String firstName;
    public String lastName;

    public User() {}

    public User(String id, String email, UserProvider provider, String firstName, String lastName) {
        this.id = id;
        this.email = email;
        this.provider = provider;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserProvider getProvider() {
        return provider;
    }

    public void setProvider(UserProvider provider) {
        this.provider = provider;
    }

    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }
}
