package com.webapps770.tweet_flow.models;

import android.arch.persistence.room.Entity;

@Entity(tableName = "tag")
public class Tag {

    public String id;
    public String name;


    public Tag() { }

    public Tag(String name) {
        this.name = name;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}