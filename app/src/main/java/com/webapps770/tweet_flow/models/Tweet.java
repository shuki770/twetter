package com.webapps770.tweet_flow.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;
import java.util.List;

import android.support.annotation.NonNull;

@Entity(tableName = "tweet")
public class Tweet {
    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "message")
    private String message;

    @ColumnInfo(name = "tag")
    private String tag;

    @ColumnInfo(name = "created_date")
    private Date createdDate;

    @ColumnInfo(name = "created_by")
    private String createdBy;

    @ColumnInfo(name = "user_id")
    private String userId;

    @ColumnInfo(name = "user_photo")
    private String userPhoto;

    @ColumnInfo(name = "image_url")
    private String imageUrl;

    public Tweet() {
    }

    public Tweet(String tweet, String imageUrl) {
        this.message = tweet;
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }


}