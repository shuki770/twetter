package com.webapps770.tweet_flow.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by shuki on 13/03/2019.
 */

@Entity(tableName = "user_tags")
public class UserTag {
    @PrimaryKey
    @NonNull
    private String tagName;

    @NonNull
    public String getTagName() {
        return tagName;
    }

    public UserTag(@NonNull String tagName) {
        this.tagName = tagName;
    }

    public void setTagName(@NonNull String tagName) {
        this.tagName = tagName;
    }


}