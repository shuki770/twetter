package com.webapps770.tweet_flow.services;

import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.webapps770.tweet_flow.db.UserTagDbProvider;
import com.webapps770.tweet_flow.models.Tag;

import org.reactivestreams.Subscriber;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public class TagsService {
    private static String TAG = "TagsService:";
    public static List<Tag> tags;
    public static BehaviorSubject<List<String>> userTags$ = BehaviorSubject.createDefault(new ArrayList<String>());
    private TagsService() {}

    public static Task<DocumentReference> addTag(Tag tag) {
        return FirebaseService.getCollection("Tags").add(tag);
    }

    public static void addUserTag(String tag) {
        List<String> userTags = userTags$.getValue();
        if (!userTags.contains(tag)) {
            userTags.add(tag);
            setUserTags(userTags);
        }
    }

    public static void removeUserTag(String tag) {
        List<String> userTags = userTags$.getValue();
        if (userTags.contains(tag)) {
            userTags.remove(tag);
            setUserTags(userTags);
        }
    }


    public static void setUserTags(List<String> userTags) {

        if (userTags.size() > 0) {
            UserTagDbProvider.upsertAll(userTags)
                    .subscribeOn(Schedulers.io())
                    .subscribe();
        } else {
            UserTagDbProvider.removeAll()
                    .subscribeOn(Schedulers.io())
                    .subscribe();
        }

        userTags$.onNext(userTags);
    }

    public static Observable<List<Tag>> getTags() {
        Observable<List<Tag>> tagsObservable = Observable.create(emitter -> {
            FirebaseService.getCollection("Tags").addSnapshotListener((QuerySnapshot snapshot, FirebaseFirestoreException e) -> {

                if (e != null) {
                    Log.e(TAG + "getTags", "addSnapshotListener error", e);
                    emitter.onError(e);
                    return;
                }

                tags = new ArrayList<>();

                for (DocumentSnapshot document : snapshot.getDocuments()) {
                    Tag tag = document.toObject(Tag.class);
                    tag.setId(document.getId());
                    tags.add(tag);
                }

                emitter.onNext(tags);

            });
        });

        return tagsObservable;

    }
}

