package com.webapps770.tweet_flow.services;

import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.webapps770.tweet_flow.db.AppDatabase;
import com.webapps770.tweet_flow.db.TweetDbProvider;
import com.webapps770.tweet_flow.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by shuki on 10/02/2019.
 */

public class TweetService {
    private static String TAG = "TweetService:";
    private static String collectionName = "Tweets";
    public static AppDatabase db = AppDatabase.getInstance();
    public static BehaviorSubject<List<Tweet>> tweets$ = BehaviorSubject.create();

    private TweetService() {
    }

    public static Task<DocumentReference> addTweet(Tweet tweet) {
        return FirebaseService.getCollection(collectionName).add(tweet);
    }

    public static void init() {
        // Listen to user tags changes
        TagsService.userTags$
                .subscribe(userTags -> {
                    TweetDbProvider.getByTags(userTags)
                            .subscribeOn(Schedulers.io())
                            .subscribe(tweets$::onNext);
                });

        // Listen to firebase db changes
        FirebaseService.getCollection(collectionName)
                .orderBy("createdDate", Query.Direction.DESCENDING)
                .addSnapshotListener((QuerySnapshot snapshot, FirebaseFirestoreException e) -> {

                    if (e != null) {
                        Log.e(TAG + ":getTweets", "addSnapshotListener error", e);
                        tweets$.onError(e);
                        return;
                    }

                    List<Tweet> tweets = new ArrayList<>();

                    for (DocumentSnapshot document : snapshot.getDocuments()) {
                        Tweet tweet = document.toObject(Tweet.class);
                        tweet.setId(document.getId());
                        tweets.add(tweet);
                    }

                    tweets$.onNext(tweets);

                    // Update local db
                     TweetDbProvider.upsertAll(tweets)
                             .subscribeOn(Schedulers.io())
                             .subscribe(t -> {
                                 List<String> userTags = TagsService.userTags$.getValue();
                                 if (userTags.size() > 0) {
                                     TagsService.setUserTags(userTags);
                                 }
                             });

                });
    }

    public static Observable<List<Tweet>> getTweets() {
        return tweets$;
    }

    public static Observable<Uri> uploadImage(Uri imageUri) {
        return Observable.create(emitter -> {
            StorageReference reference = FirebaseStorage.getInstance().getReference("tweets/" + System.currentTimeMillis() + ".jpeg");
            UploadTask uploadTask = reference.putFile(imageUri);
            uploadTask
                    .continueWithTask(task -> {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return reference.getDownloadUrl();
                    })
                    .addOnFailureListener(emitter::onError)
                    .addOnSuccessListener(uri -> {
                        emitter.onNext(uri);
                    });

        });
    }

}
