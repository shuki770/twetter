package com.webapps770.tweet_flow.services;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

public class FirebaseService {

    private static FirebaseFirestore database = FirebaseFirestore.getInstance();
    static {
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        database.setFirestoreSettings(settings);
    }
    static public CollectionReference getCollection(String collection) {
        return database.collection(collection);
    }
}
