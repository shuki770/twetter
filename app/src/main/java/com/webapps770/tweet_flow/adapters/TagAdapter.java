package com.webapps770.tweet_flow.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.webapps770.tweet_flow.R;
import com.webapps770.tweet_flow.models.Tag;
import com.webapps770.tweet_flow.services.TagsService;

import java.util.ArrayList;
import java.util.List;


public class TagAdapter extends RecyclerView.Adapter<TagAdapter.ViewHolder> {
    private List<Tag> tags;

    public TagAdapter(List<Tag> tags) {
        this.tags = tags;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        CheckBox tagCheckBox;


        public ViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.txt_p_name);
            tagCheckBox = itemView.findViewById(R.id.checkBox);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tag_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Tag tag = this.tags.get(position);

        holder.tvName.setText(tag.getName());

        if (TagsService.userTags$.getValue().contains(tag.name)) {
            holder.tagCheckBox.setChecked(true);
        }

        holder.tagCheckBox.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked) -> {
                if (isChecked) {
                    TagsService.addUserTag(tag.getName());
                } else {
                    TagsService.removeUserTag(tag.getName());
                }
        });

    }

    @Override
    public int getItemCount() {
        return tags.size();
    }
}



