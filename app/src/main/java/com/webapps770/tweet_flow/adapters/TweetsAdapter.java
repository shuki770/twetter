package com.webapps770.tweet_flow.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.webapps770.tweet_flow.R;
import com.webapps770.tweet_flow.fragments.TweetsFragment;
import com.webapps770.tweet_flow.models.Tweet;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by shuki on 10/02/2019.
 */
public class TweetsAdapter extends RecyclerView.Adapter<TweetsAdapter.ViewHolder> {
    private List<Tweet> tweets;

    public TweetsAdapter(List<Tweet> tweets) {
        this.tweets = tweets;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCreatedBy;
        TextView tvCreatedDate;
        TextView tvMessage;
        TextView tvTag;
        ImageView img;
        ImageView userImage;

        public ViewHolder(View itemView) {
            super(itemView);

            tvCreatedBy = itemView.findViewById(R.id.text_tweet_create_by);
            tvCreatedDate = itemView.findViewById(R.id.text_tweet_create_date);
            tvTag = itemView.findViewById(R.id.tv_tweet_tag);
            tvMessage = itemView.findViewById(R.id.tv_tweet_message);
            img = itemView.findViewById(R.id.img_tweet_message_image);
            userImage = itemView.findViewById(R.id.user_image);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tweet_item,  parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Tweet tweet = this.tweets.get(position);

        holder.tvMessage.setText(tweet.getMessage());

        if (tweet.getCreatedBy() != null) {
            holder.tvCreatedBy.setText(tweet.getCreatedBy());
        }


        if (tweet.getCreatedDate() != null) {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            holder.tvCreatedDate.setText(format.format(tweet.getCreatedDate()));
        }

        if (!TextUtils.isEmpty(tweet.getTag())) {

            holder.tvTag.setText( Html.fromHtml("<u>" + tweet.getTag() + "</u>"));
        }

        if (tweet.getImageUrl() != null) {
            Glide.with(holder.img.getContext()).load(tweet.getImageUrl()).into(holder.img);
        } else {
            holder.img.setVisibility(View.GONE);
        }


        Glide.with(holder.img.getContext())
                .load(tweet.getUserPhoto())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.userImage);
    }

    @Override
    public int getItemCount() {
        return tweets.size();
    }
}



